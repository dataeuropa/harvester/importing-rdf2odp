# ChangeLog

## Unreleased

## 1.0.7 (2024-05-13)

**Changed:**
* Configurable HTTP body size limit

## 1.0.6 (2023-11-16)

**Changed:**
* General dependency updates

## 1.0.5 (2023-06-05)

**Changed:**
* Formatting Markdown output
* Create action reports new location URI
* Use IO dispatcher for worker verticle

## 1.0.3 (2023-03-11)

**Added:**
* Info log output when start and end processing of uploaded packages

**Changed:**
* Use new hub API 

## 1.0.2 (2022-11-22)

**Changed:**
* Filename validation (being more tolerant) 

**Added:**
* Additional odp protocol versions 1.0, 2.0, 3.1, 3.2

## 1.0.1 (2022-11-22)

**Added:**
* `deleteWaste`parameter to deletes waste in a catalogue

**Changed:**
* Use legacy hash stringify method (this method is critical!)

## 1.0.0 (2022-11-21)

Initial release
