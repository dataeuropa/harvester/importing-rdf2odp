package io.piveau.consus.importing

import io.piveau.consus.importing.rdf2odp.ImportingRdf2odpVerticle
import io.piveau.consus.importing.security.DatasetPermissionHandler
import io.piveau.consus.importing.security.SCOPE_DATASET_CREATE
import io.piveau.consus.importing.security.SCOPE_DATASET_DELETE
import io.piveau.consus.importing.security.SCOPE_DATASET_UPDATE
import io.piveau.pipe.connector.PipeConnector
import io.piveau.security.ApiKeyAuthProvider
import io.piveau.security.PiveauAuth
import io.piveau.security.PiveauAuthConfig
import io.vertx.config.ConfigRetriever
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.*
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.healthchecks.HealthCheckHandler
import io.vertx.ext.healthchecks.Status
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.APIKeyHandler
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.openapi.RouterBuilder
import io.vertx.ext.web.validation.RequestParameters
import io.vertx.ext.web.validation.ValidationHandler
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class MainVerticle : CoroutineVerticle() {

    private val log: Logger = LoggerFactory.getLogger(javaClass)

    override suspend fun start() {
        val envStoreOptions = ConfigStoreOptions()
            .setType("env")
            .setConfig(
                JsonObject().put(
                    "keys", JsonArray()
                        .add("PIVEAU_CLUSTER_CONFIG")
                        .add("PIVEAU_HUB_REPO_ADDRESS")
                        .add("PIVEAU_HUB_REPO_APIKEY")
                        .add("PIVEAU_RDF2ODP_AUTH_CONFIG")
                        .add("PIVEAU_RDF2ODP_HTTP_BODY_LIMIT")
                )
            )

        val fileStoreOptions = ConfigStoreOptions()
            .setType("directory")
            .setConfig(
                JsonObject()
                    .put("path", "conf")
                    .put(
                        "filesets", JsonArray()
                            .add(
                                JsonObject()
                                    .put("format", "json")
                                    .put("pattern", "*.json")
                            )
                            .add(
                                JsonObject()
                                    .put("format", "yaml")
                                    .put("pattern", "*.yaml")
                            )
                            .add(
                                JsonObject()
                                    .put("format", "yaml")
                                    .put("pattern", "*.yml")
                            )
                            .add(
                                JsonObject()
                                    .put("format", "properties")
                                    .put("pattern", "*.properties")
                            )
                    )
            )

        val retriever = ConfigRetriever.create(
            vertx, ConfigRetrieverOptions()
                .addStore(fileStoreOptions)
                .addStore(envStoreOptions)
        )

        val importerConfig = retriever.config.await()
        log.info(importerConfig.encodePrettily())

        vertx.deployVerticle(
            ImportingRdf2odpVerticle::class.java,
            DeploymentOptions().setConfig(importerConfig).setWorker(true)
        ).await()
        val connector = PipeConnector.create(vertx, DeploymentOptions()).await()

        connector.publishTo(ImportingRdf2odpVerticle.PIPE_ADDRESS)

        val routerBuilder = RouterBuilder.create(vertx, "webroot/openapi-rdf2odp.yaml").await()

        val bodyLimit = importerConfig.getLong("PIVEAU_RDF2ODP_HTTP_BODY_LIMIT", BodyHandler.DEFAULT_BODY_LIMIT)
        routerBuilder.rootHandler(BodyHandler.create().setBodyLimit(bodyLimit))

        val authConfig = importerConfig.getJsonObject("PIVEAU_RDF2ODP_AUTH_CONFIG", JsonObject())

        val apiKeys = authConfig.getJsonObject("apiKeys", JsonObject().put("apiKey", JsonArray().add("*")))
            .filterIsInstance<Map.Entry<String, JsonArray>>()
            .associate { (key, resource) -> key to resource.filterIsInstance<String>() }

        routerBuilder.securityHandler("ApiKeyAuth", APIKeyHandler.create(ApiKeyAuthProvider(apiKeys)))

        val piveauAuth = PiveauAuth.create(vertx, PiveauAuthConfig(authConfig)).await()
        routerBuilder.securityHandler("BearerAuth", piveauAuth.authHandler())

        routerBuilder.operation("postPackage")
            .handler {
                DatasetPermissionHandler(
                    listOf(
                        SCOPE_DATASET_CREATE,
                        SCOPE_DATASET_UPDATE,
                        SCOPE_DATASET_DELETE
                    )
                ).handle(it)
            }
            .handler { handleUploadPackage(it) }

        val router = routerBuilder.createRouter()
        router.route("/*").handler(StaticHandler.create())

        val hch = HealthCheckHandler.create(vertx)
        hch.register("buildInfo") { future: Promise<Status?> ->
            vertx.fileSystem().readFile("buildInfo.json")
                .onSuccess { bi -> future.complete(Status.OK(bi.toJsonObject())) }
                .onFailure(future::fail)
        }
        router.get("/health").handler(hch)

        connector.subRouter("/ingestion/*", router)
    }

    private fun handleUploadPackage(context: RoutingContext) {
        val params = context.get<RequestParameters>(ValidationHandler.REQUEST_CONTEXT_KEY)
        val validateOnly = params.queryParameter("validateOnly").boolean
        val deleteWaste = params.queryParameter("deleteWaste").boolean

        val futures = context.fileUploads()
            .map { fileUpload ->
                log.info("Start processing ${fileUpload.fileName()}...")
                vertx.eventBus().request<String>(
                    ImportingRdf2odpVerticle.UPLOAD_ADDRESS,
                    JsonObject()
                        .put("uploadedFileName", fileUpload.uploadedFileName())
                        .put("fileName", fileUpload.fileName())
                        .put("validateOnly", validateOnly)
                        .put("deleteWaste", deleteWaste),
                    DeliveryOptions().setSendTimeout(300000) // 5 min timeout
                )
            }
            .toList()

        CompositeFuture.join(futures)
            .onComplete {
                var response = "Packages processed: ${futures.size}\n\n---\n\n"

                futures.forEach {
                    response += if (it.succeeded()) {
                        log.info("Processing finished")
                        it.result().body()
                    } else {
                        log.error("Processing failed: ${it.cause().message}")
                        "ERROR: ${it.cause().message}\n"
                    }
                }
                context.response().setStatusCode(200).putHeader(HttpHeaders.CONTENT_TYPE, "text/markdown").end(response)
            }
    }
}

fun main(args: Array<String>) {
    Launcher.executeCommand("run", *(args.plus(MainVerticle::class.java.name)))
}
