package io.piveau.consus.importing.actions

import io.piveau.consus.importing.rdf2odp.euodpHash
import org.jdom2.Element
import org.jdom2.Namespace
import org.jdom2.filter.Filters
import org.jdom2.xpath.XPathExpression
import org.jdom2.xpath.XPathFactory

sealed class Action(element: Element, namespace: Namespace) {

    val id: String = element.getAttributeValue("id", namespace)
    val objectUri: String = element.getAttributeValue("object-uri", namespace)
    val objectType: String? = element.getAttributeValue("object-type", namespace)
    private val ckanName: String? = element.getAttributeValue("object-ckan-name", namespace)

    val objectId
        get() = ckanName ?: objectUri.euodpHash()

    companion object {
        fun create(element: Element, namespace: Namespace) : Action {
            val xpathExpression : XPathExpression<Element> = XPathFactory.instance().compile("ecodp:*", Filters.element(), mapOf(), listOf(namespace))
            val action = xpathExpression.evaluateFirst(element)
            return when (action.name) {
                "add-replace" -> AddReplace(element, action, namespace)
                "remove" -> Remove(element, namespace)
                "upload-distribution" -> UploadDistribution(element, action, namespace)
                "change-status" -> ChangeStatus(element, action, namespace)
                else -> GenericAction(element, namespace)
            }
        }
    }
}