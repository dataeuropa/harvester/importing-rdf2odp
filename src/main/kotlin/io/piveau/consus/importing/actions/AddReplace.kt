package io.piveau.consus.importing.actions

import org.jdom2.Element
import org.jdom2.Namespace

class AddReplace(element: Element, action: Element, namespace: Namespace) : Action(element, namespace) {
    val objectStatus: String = action.getAttributeValue("object-status", namespace)
    val packagePath: String = action.getAttributeValue("package-path", namespace)
    val generateDoi: String? = action.getAttributeValue("generate-doi", namespace)
}
