package io.piveau.consus.importing.actions

import org.jdom2.Element
import org.jdom2.Namespace

class ChangeStatus(element: Element, action: Element, namespace: Namespace) : Action(element, namespace) {
    val objectStatus: String = action.getAttributeValue("object-status", namespace)
}