package io.piveau.consus.importing.actions

import org.jdom2.Element
import org.jdom2.Namespace

class UploadDistribution(element: Element, action: Element, namespace: Namespace) : Action(element, namespace) {
    val packagePath: String = action.getAttributeValue("package-path", namespace)
}