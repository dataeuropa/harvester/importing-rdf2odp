package io.piveau.consus.importing.hub

import io.piveau.rdf.RDFMimeTypes
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.predicate.ResponsePredicate

class HubExporter(private val client: WebClient, private val address: String, private val apiKey: String) {

    fun putDataset(dataset: Buffer, id: String, catalogue: String): Future<JsonObject> = Future.future { promise ->
        client.putAbs("$address/catalogues/$catalogue/datasets/origin")
            .timeout(5000)
            .putHeader("X-API-Key", apiKey)
            .putHeader(HttpHeaders.CONTENT_TYPE.toString(), RDFMimeTypes.RDFXML)
            .addQueryParam("originalId", id)
            .sendBuffer(dataset)
            .onSuccess { response ->
                val status = JsonObject()
                when (response.statusCode()) {
                    200, 204 -> promise.complete(status.put("result", "updated"))
                    201 -> promise.complete(status
                        .put("result", "created")
                        .put("location", response.getHeader("Location")))
                    304 -> promise.complete(status.put("result", "skipped"))
                    401 -> promise.fail("not authenticated")
                    403 -> promise.fail("forbidden")
                    400 -> promise.fail("bad request")
                    else -> promise.fail(response.statusMessage())
                }
            }
            .onFailure(promise::fail)
    }

    fun deleteDataset(id: String, catalogue: String): Future<Void> = Future.future { promise ->
        client.deleteAbs("$address/catalogues/$catalogue/datasets/origin")
            .putHeader("X-API-Key", apiKey)
            .addQueryParam("originalId", id)
            .expect(ResponsePredicate.SC_SUCCESS)
            .send()
            .onSuccess { promise.complete() }
            .onFailure(promise::fail)
    }

    fun getDatasetIds(catalogue: String): Future<Set<String>> = Future.future { promise ->
        client.getAbs("$address/catalogues/$catalogue/datasets")
            .putHeader("X-API-Key", apiKey)
            .addQueryParam("valueType", "originalIds")
            .expect(ResponsePredicate.SC_SUCCESS)
            .send()
            .onSuccess {
                val ids = it.body().toJsonArray()
                    .map(Any::toString)
                    .toSet()
                promise.complete(ids)
            }
            .onFailure(promise::fail)
    }

}