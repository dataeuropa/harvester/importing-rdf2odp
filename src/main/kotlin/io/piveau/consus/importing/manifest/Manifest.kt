package io.piveau.consus.importing.manifest

import io.piveau.consus.importing.actions.Action
import io.piveau.consus.importing.rdf2odp.PackageReport
import io.piveau.consus.importing.rdf2odp.Phase
import io.piveau.vocabularies.loadResource
import io.vertx.core.json.JsonObject
import org.jdom2.Document
import org.jdom2.Element
import org.jdom2.Namespace
import org.jdom2.filter.Filters
import org.jdom2.transform.JDOMSource
import org.jdom2.xpath.XPathExpression
import org.jdom2.xpath.XPathFactory
import org.xml.sax.ErrorHandler
import org.xml.sax.InputSource
import org.xml.sax.SAXParseException
import java.io.File
import javax.xml.transform.Source
import javax.xml.transform.SourceLocator
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory
import javax.xml.validation.Validator

val schemaNamespace: Namespace = Namespace.getNamespace("http://www.w3.org/2001/XMLSchema-instance")

val odpVersionContextV10 = VersionContext(
    Namespace.getNamespace("ecodp", "http://data.europa.eu/euodp/ontologies/ec-odp#"),
    SchemaFactory.newDefaultInstance()
        .newSchema(StreamSource(object {}.javaClass.classLoader.getResourceAsStream("odp-protocol-v1.0.xsd")))
        .newValidator()
)

val odpVersionContextV20 = VersionContext(
    Namespace.getNamespace("ecodp", "http://data.europa.eu/euodp/ontologies/ec-odp#"),
    SchemaFactory.newDefaultInstance()
        .newSchema(StreamSource(object {}.javaClass.classLoader.getResourceAsStream("odp-protocol-v2.0.xsd")))
        .newValidator()
)

val odpVersionContextV21 = VersionContext(
    Namespace.getNamespace("ecodp", "http://data.europa.eu/euodp/ontologies/ec-odp#"),
    SchemaFactory.newDefaultInstance()
        .newSchema(StreamSource(object {}.javaClass.classLoader.getResourceAsStream("odp-protocol-v2.1.xsd")))
        .newValidator()
)

val odpVersionContextV30 = VersionContext(
    Namespace.getNamespace("ecodp", "http://data.europa.eu/88u/ontology/dcatapop#"),
    SchemaFactory.newDefaultInstance()
        .newSchema(StreamSource(object {}.javaClass.classLoader.getResourceAsStream("odp-protocol-v3.0.xsd")))
        .newValidator()
)

val odpVersionContextV31 = VersionContext(
    Namespace.getNamespace("ecodp", "http://data.europa.eu/88u/ontology/dcatapop#"),
    SchemaFactory.newDefaultInstance()
        .newSchema(StreamSource(object {}.javaClass.classLoader.getResourceAsStream("odp-protocol-v3.1.xsd")))
        .newValidator()
)

val odpVersionContextV32 = VersionContext(
    Namespace.getNamespace("ecodp", "http://data.europa.eu/88u/ontology/dcatapop#"),
    SchemaFactory.newDefaultInstance()
        .newSchema(StreamSource(object {}.javaClass.classLoader.getResourceAsStream("odp-protocol-v3.2.xsd")))
        .newValidator()
)

val odpVersionContextV40 = VersionContext(
    Namespace.getNamespace("ecodp", "http://data.europa.eu/88u/ontology/dcatapop#"),
    SchemaFactory.newDefaultInstance()
        .newSchema(StreamSource(object {}.javaClass.classLoader.getResourceAsStream("odp-protocol-v4.0.xsd")))
        .newValidator()
)

val odpVersionContextV41 = VersionContext(
    Namespace.getNamespace("ecodp", "http://data.europa.eu/88u/ontology/dcatapop#"),
    SchemaFactory.newDefaultInstance()
        .newSchema(StreamSource(object {}.javaClass.classLoader.getResourceAsStream("odp-protocol-v4.1.xsd")))
        .newValidator()
)

val odpVersionContextV42 = VersionContext(
    Namespace.getNamespace("ecodp", "http://data.europa.eu/88u/ontology/dcatapop#"),
    SchemaFactory.newDefaultInstance()
        .newSchema(StreamSource(object {}.javaClass.classLoader.getResourceAsStream("odp-protocol-v4.2.xsd")))
        .newValidator()
)

fun Document.actions(): Sequence<Action> = odpVersion().let { version ->
    version.actionsXPathExpression.evaluate(this).map { Action.create(it, version.namespace) }.asSequence()
}

fun Document.metaInfo(): JsonObject {
    val metaInfo = JsonObject()
    rootElement.attributes
        .filter { it.namespace == odpVersion().namespace }
        .forEach {
            when (it.name) {
                "version" -> metaInfo.put("version", it.value)
                "package-id" -> metaInfo.put("packageId", it.value)
                "creation-date-time" -> metaInfo.put("creationDateTime", it.value)
                "publisher" -> metaInfo.put("publisher", it.value)
                "priority" -> metaInfo.put("priority", it.value)
            }
        }
    return metaInfo
}

class ReportErrorHandler(private val report: PackageReport) : ErrorHandler {
    override fun warning(exception: SAXParseException) {
        report.addWarning(Phase.Validation, "Manifest validation: ${exception.message}")
    }

    override fun error(exception: SAXParseException) {
        report.addError(Phase.Validation, "Manifest validation: ${exception.message}")
    }

    override fun fatalError(exception: SAXParseException) {
        report.addFatal(Phase.Validation, "Manifest validation: ${exception.message}")
    }
}

fun validateManifest(manifest: Document): Boolean {
    val schemaLocation = manifest.rootElement.getAttribute("schemaLocation", schemaNamespace)?.value ?: ""
    return try {
        val version = schemaLocation.mapValidator() ?: odpVersionContextV10
        version.validator.validate(JDOMSource(manifest)).let { true }
    } catch (e: Exception) {
        false
    }
}

fun validateManifest(manifest: Document, report: PackageReport) {
    val schemaLocation = manifest.rootElement.getAttribute("schemaLocation", schemaNamespace)?.value ?: ""
    val version = schemaLocation.mapValidator() ?: odpVersionContextV10.also {
        report.addWarning(
            Phase.Validation,
            "Unknown manifest schema version $schemaLocation, using 1.0 for manifest validation"
        )
    }
    with(version.validator) {
        val previousErrorHandler = errorHandler
        errorHandler = ReportErrorHandler(report)
        validate(JDOMSource(manifest))
        errorHandler = previousErrorHandler
    }
}

fun Document.odpVersion(): VersionContext {
    val schemaLocation = rootElement.getAttribute("schemaLocation", schemaNamespace)?.value ?: ""
    return schemaLocation.mapValidator() ?: odpVersionContextV21
}

private fun String.mapValidator(): VersionContext? = when {
    contains("protocol-v4.2") -> odpVersionContextV42
    contains("protocol-v4.1") -> odpVersionContextV41
    contains("protocol-v4.0") -> odpVersionContextV40
    contains("protocol-v3.2") -> odpVersionContextV30
    contains("protocol-v3.1") -> odpVersionContextV30
    contains("protocol-v3.0") -> odpVersionContextV30
    contains("protocol-v2.1") -> odpVersionContextV21
    contains("protocol-v2.0") -> odpVersionContextV20
    contains("protocol-v1.0") -> odpVersionContextV10
    else -> null
}

class VersionContext(val namespace: Namespace, val validator: Validator) {
    val actionsXPathExpression: XPathExpression<Element> =
        XPathFactory.instance().compile("ecodp:manifest/ecodp:action", Filters.element(), mapOf(), listOf(namespace))
}
