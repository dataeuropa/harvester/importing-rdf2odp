package io.piveau.consus.importing.rdf2odp

import io.piveau.consus.importing.actions.AddReplace
import io.piveau.consus.importing.actions.ChangeStatus
import io.piveau.consus.importing.actions.Remove
import io.piveau.consus.importing.actions.UploadDistribution
import io.piveau.consus.importing.hub.HubExporter
import io.piveau.consus.importing.manifest.actions
import io.piveau.consus.importing.manifest.metaInfo
import io.piveau.consus.importing.store.StoreExporter
import io.piveau.consus.importing.zip.getObject
import io.piveau.consus.importing.zip.manifest
import io.piveau.consus.importing.zip.validatePackage
import io.piveau.json.ConfigHelper.Companion.forConfig
import io.piveau.pipe.PipeContext
import io.piveau.pipe.PipeLauncher
import io.piveau.pipe.PiveauCluster.Companion.create
import io.piveau.vocabularies.CorporateBodies
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.apache.jena.rdf.model.ModelFactory
import org.jdom2.Document
import org.slf4j.LoggerFactory
import java.math.BigInteger
import java.security.MessageDigest
import java.util.Base64
import java.util.zip.ZipFile
import kotlin.coroutines.CoroutineContext
import kotlin.io.path.Path

class ImportingRdf2odpVerticle : CoroutineVerticle() {

    private val log = LoggerFactory.getLogger(javaClass)

    private lateinit var launcher: PipeLauncher
    private lateinit var hubExporter: HubExporter
    private lateinit var storeExporter: StoreExporter

    override suspend fun start() {

        val clusterConfig = forConfig(config).forceJsonObject("PIVEAU_CLUSTER_CONFIG")

        val cluster = create(vertx, clusterConfig).await()
        launcher = cluster.pipeLauncher(vertx).await()

        hubExporter = HubExporter(
            WebClient.create(vertx),
            config.getString("PIVEAU_HUB_REPO_ADDRESS", "http://piveau-hub-repo:8080"),
            config.getString("PIVEAU_HUB_REPO_APIKEY", "apiKey")
        )

        storeExporter = StoreExporter(
            WebClient.create(vertx),
            config.getString("PIVEAU_HUB_STORE_ADDRESS", "http://piveau-hub-store:8080")
        )

        vertx.eventBus().consumer(PIPE_ADDRESS) { message ->
            launch(Dispatchers.IO) { handlePipe(message.body()) }
        }
        vertx.eventBus().consumer(UPLOAD_ADDRESS) { message ->
            launch(Dispatchers.IO) { handlePackage(message) }
        }
    }

    private suspend fun handlePipe(pipeContext: PipeContext) {

    }

    private suspend fun handlePackage(message: Message<JsonObject>) {

        val packageInfo = message.body()
        val report = validatePackage(packageInfo)

        val deleteWaste = packageInfo.getBoolean("deleteWaste", false)

        if (packageInfo.getBoolean("validateOnly")) {
//            storeReport(vertx, report)
            return message.reply(report.prettyPrint())
        }

        if (report.packageStatus == "fatal") {
            return message.reply(report.prettyPrint())
        }

        val idSet = mutableSetOf<String>()

//        withContext(Dispatchers.IO) {
        ZipFile(Path(packageInfo.getString("uploadedFileName")).toFile()).use { zipFile ->

            val manifest = zipFile.manifest() as Document

            val publisher = manifest.metaInfo().getString("publisher")
            val catalogue =
                CorporateBodies.getConcept(ModelFactory.createDefaultModel().createResource(publisher))?.identifier?.lowercase()
                    ?: publisher.substringAfterLast("/").lowercase()

            // For many action we may better serialize them

            manifest.actions().forEach { action ->
                when (action) {
                    is AddReplace -> {
                        val dataset = zipFile.getObject(action.packagePath)
                        if (dataset != null) {
                            if (log.isTraceEnabled) {
                                log.trace("Processing unzipped dataset:\n{}", dataset.toString())
                            }

                            val id = action.objectId
                            try {
                                val result = hubExporter.putDataset(dataset, id, catalogue).await()
                                idSet.add(id)
                                report.addInfo(
                                    Phase.Processing,
                                    "Dataset ${action.objectUri} ($id) ${result.encode()}"
                                )
                            } catch (e: Throwable) {
                                log.error("Hub put dataset", e)
                                report.addError(
                                    Phase.Processing,
                                    "Hub create/update ${action.objectUri}: ${e.message}"
                                )
                            }
                        }
                    }

                    is Remove -> {
                        val id = action.objectId
                        try {
                            hubExporter.deleteDataset(id, catalogue).await()
                            report.addInfo(Phase.Processing, "Dataset ${action.objectUri} deleted")
                        } catch (e: Throwable) {
                            log.error("Hub delete dataset", e)
                            report.addError(
                                Phase.Processing,
                                "Hub delete ${action.objectUri}: ${e.message}"
                            )
                        }
                    }

                    is UploadDistribution -> {
                        val data = zipFile.getObject(action.packagePath)
                        if (data != null) {
//                                val location = storeExporter.storeDistribution(data, catalogue, "token")
                            report.addWarning(Phase.Processing, "Upload is under construction. Ignoring ${action.id} for now.")
                        }
                    }

                    is ChangeStatus -> {
                        report.addWarning(
                            Phase.Processing,
                            "Status change is under construction. Ignoring ${action.id} for now."
                        )
                    }

                    else -> {
                        report.addWarning(Phase.Processing, "Unknown action. Ignoring ${action.id}.")
                    }
                }
            }

            if (deleteWaste) {
                log.debug("Processed ${idSet.size} datasets.")
                deleteDatasets(catalogue, idSet)
            }
        }
//        }

        log.debug("Reply with report")
        message.reply(report.prettyPrint())
    }

    private suspend fun deleteDatasets(catalogue: String, idSet: Set<String>) {
        val set = try {
            hubExporter.getDatasetIds(catalogue).await()
        } catch (t: Throwable) {
            log.error("Fetching {} dataset identifiers", catalogue, t)
            return
        }

        log.debug("$catalogue has ${set.size} datasets.")

        val all = set - idSet

        log.debug("Deleting ${all.size} datasets now")

        all.forEach {
            try {
                hubExporter.deleteDataset(it, catalogue).await()
                log.trace("Deleted {}", it)
            } catch (t: Throwable) {
                log.error("Deletion of {} failed", it, t)
            }
        }
        log.debug("Finished deletion")
    }

    companion object {
        const val PIPE_ADDRESS = "io.piveau.pipe.importing.rdf2odp.queue"
        const val UPLOAD_ADDRESS = "io.piveau.package.importing.rdf2odp.queue"
    }
}

suspend fun storeReport(vertx: Vertx, report: PackageReport) {
    if (!vertx.fileSystem().exists("reports").await()) {
        vertx.fileSystem().mkdir("reports").await()
    }
    val reportFileName = report.fileName.removeSuffix(".zip") + ".md"
    vertx.fileSystem()
        .writeFile(
            Path("reports").resolve(reportFileName).toString(),
            Buffer.buffer(report.prettyPrint())
        )
        .await()
}

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray(Charsets.UTF_8))).toString(16)
        .padStart(32, '0')
}

fun String.euodpHash(): String {
    val md = MessageDigest.getInstance("MD5")
    return Base64.getEncoder()
        .encodeToString(md.digest(toByteArray(Charsets.UTF_8)))
        .replace("[^a-zA-Z0-9]".toRegex(), "")
}