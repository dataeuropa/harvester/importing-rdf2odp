package io.piveau.consus.importing.rdf2odp

import io.vertx.core.json.JsonObject
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

private fun Instant.dateTimeFormat(): String = DateTimeFormatter.ISO_DATE_TIME.format(atOffset(ZoneOffset.UTC).truncatedTo(ChronoUnit.SECONDS))
private fun Instant.timeFormat(): String = DateTimeFormatter.ISO_TIME.format(atOffset(ZoneOffset.UTC).truncatedTo(ChronoUnit.MILLIS))

enum class Phase {
    Validation, Processing
}

data class ReportEntry(val timestamp: Instant, val phase: Phase, val severity: String, val message: String)

class PackageReport(info: JsonObject) {
    val fileName: String = info.getString("fileName")

    private val entries = mutableListOf<ReportEntry>()

    fun addInfo(phase: Phase, message: String) = add(ReportEntry(Instant.now(), phase, "INFO", message))
    fun addWarning(phase: Phase, message: String) = add(ReportEntry(Instant.now(), phase,  "WARNING", message))
    fun addError(phase: Phase, message: String) = add(ReportEntry(Instant.now(), phase, "ERROR", message))
    fun addFatal(phase: Phase, message: String) = add(ReportEntry(Instant.now(), phase, "FATAL", message))

    fun add(entry: ReportEntry) = entries.add(entry)

    fun prettyPrint(): String {
        val output = StringBuilder()
        output.append("""# Package report for $fileName
            |
            |Validation time: ${Instant.now().dateTimeFormat()}
            |
        """.trimMargin()
        )
        if (packageStatus != "fatal") {
            output.append("\nStatus: Package is processable!\n")
        }
        output.append("\n${entries.count { it.severity == "FATAL" }} fatal, ${entries.count { it.severity == "ERROR" }} errors, ${entries.count { it.severity == "WARNING" }} warnings, ${entries.count { it.severity == "INFO" }} infos.\n")

        entries.sortBy { it.timestamp }

        output.append("\n---\n\n## Validation\n\n")
        if (entries.none { it.phase == Phase.Validation }) {
            output.append("Package is fully valid!\n")
        } else {
            entries.filter { it.phase == Phase.Validation }.forEach {
                output.append("* [${it.timestamp.timeFormat()}] [${it.severity}] ${it.message}\n")
            }
        }

        if (entries.any { it.phase == Phase.Processing }) {
            output.append("\n---\n\n## Processing\n\n")
            entries.filter { it.phase == Phase.Processing }.forEach {
                output.append("* [${it.timestamp.timeFormat()}] [${it.severity}] ${it.message}\n")
            }
        } else if (packageStatus == "fatal") {
            output.append("\n---\n\n## Processing\n\n")
            output.append("Package unprocessable!\n")
        }
        return output.append("\n").toString()
    }

    val packageStatus
        get() = when {
            entries.any { it.severity == "FATAL" } -> "fatal"
            entries.any { it.severity == "ERROR" } -> "errors"
            entries.any { it.severity == "WARNING" } -> "warning"
            else -> "valid"
        }

}
