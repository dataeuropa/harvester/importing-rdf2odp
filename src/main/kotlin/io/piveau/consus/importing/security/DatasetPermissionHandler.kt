package io.piveau.consus.importing.security

import io.piveau.consus.importing.zip.decomposeParts
import io.piveau.security.PiveauAuth
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

class DatasetPermissionHandler(private val scopes: List<String>) : Handler<RoutingContext> {
    override fun handle(context: RoutingContext) {
        val resources = context.fileUploads().map {
            it.fileName().decomposeParts().first
        }.filter { it.isNotBlank() }

        if (context.user().principal().containsKey("resources")) {
            val assignedResources = context.user().principal().getJsonArray("resources").map { it as String }
            when {
                assignedResources.contains("*") -> context.next()
                assignedResources.containsAll(resources) -> context.next()
                else -> context.fail(403)
            }
        } else if (PiveauAuth.userHasRole(context.user(), "operator") || scopes.stream().allMatch { scope ->
                resources.all { resource ->
                    PiveauAuth.userHasPermission(
                        context.user(),
                        resource,
                        scope
                    )
                }
            }
        ) {
            context.next()
        } else {
            context.fail(403)
        }
    }
}