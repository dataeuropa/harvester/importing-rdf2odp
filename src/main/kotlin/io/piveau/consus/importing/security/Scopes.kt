package io.piveau.consus.importing.security

const val SCOPE_DATASET_CREATE = "dataset:create"
const val SCOPE_DATASET_UPDATE = "dataset:update"
const val SCOPE_DATASET_DELETE = "dataset:delete"
