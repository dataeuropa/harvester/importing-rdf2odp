package io.piveau.consus.importing.store

import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.await

class StoreExporter(private val client: WebClient, private val address: String) {

    suspend fun storeDistribution(distribution: Buffer, catalogue: String, token: String): String {
        client.putAbs(address)
            .bearerTokenAuthentication(token)
            .addQueryParam("catalogue", catalogue)
            .sendBuffer(distribution).await()

        return ""
    }

}