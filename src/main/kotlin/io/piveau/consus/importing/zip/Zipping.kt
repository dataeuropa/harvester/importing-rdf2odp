package io.piveau.consus.importing.zip

import io.piveau.consus.importing.actions.AddReplace
import io.piveau.consus.importing.actions.Remove
import io.piveau.consus.importing.actions.UploadDistribution
import io.piveau.consus.importing.manifest.actions
import io.piveau.consus.importing.manifest.metaInfo
import io.piveau.consus.importing.manifest.validateManifest
import io.piveau.consus.importing.rdf2odp.PackageReport
import io.piveau.consus.importing.rdf2odp.Phase
import io.piveau.rdf.toModel
import io.piveau.vocabularies.CorporateBodies
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.Lang
import org.apache.jena.riot.system.ErrorHandler
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.RDF
import org.jdom2.input.SAXBuilder
import java.util.zip.ZipException
import java.util.zip.ZipFile
import java.util.zip.ZipInputStream
import kotlin.io.path.Path

val fileNameRegex = """^([A-Z]+)_(\d{14})\.zip$""".toRegex()
val fileNameGroupsRegex = """^([A-Z]+)_(\d+)(.*)?\.zip$""".toRegex()

fun ZipFile.manifest() = getEntry("manifest.xml")?.let {
    SAXBuilder().build(getInputStream(it))
}

fun ZipFile.getObject(path: String) = getEntry(path.removePrefix("/"))?.let {
    Buffer.buffer(getInputStream(it).readAllBytes())
}

fun createZipInputStream(buffer: Buffer) = ZipInputStream(buffer.bytes.inputStream())

class ReportErrorHandler(private val report: PackageReport, private val prefix: String = "") : ErrorHandler {

    var valid = true

    override fun warning(p0: String, p1: Long, p2: Long) {
        report.addWarning(Phase.Validation, "${prefix}RDF validation [line: $p1, col: $p2]: $p0")
    }

    override fun error(p0: String, p1: Long, p2: Long) {
        report.addError(Phase.Validation, "${prefix}RDF validation [line: $p1, col: $p2]: $p0")
        valid = false
    }

    override fun fatal(p0: String, p1: Long, p2: Long) {
        report.addError(Phase.Validation, "${prefix}RDF validation [line: $p1, col: $p2]: $p0")
        valid = false
    }
}

fun validatePackage(packageInfo: JsonObject): PackageReport {
    val report = PackageReport(packageInfo)

    // Valid filename?
    val fileName: String = packageInfo.getString("fileName")

    if (!fileName.validPackageFileName()) {
        report.addWarning(Phase.Validation, "Invalid package filename $fileName")
    }
    val (publisher, _) = fileName.decomposeParts()
    val publisherConcept = if (publisher.isNotBlank()) CorporateBodies.findConcept(publisher) else null
    when {
        publisher.isBlank() -> report.addWarning(Phase.Validation, "No publisher in filename.")
        publisherConcept == null -> report.addWarning(Phase.Validation, "Publisher $publisher in filename is not a corporate body.")
    }

    try {
        ZipFile(Path(packageInfo.getString("uploadedFileName")).toFile()).use { zipFile ->

            // Checks:
            // 1. Manifest available
            val manifest = zipFile.manifest()
            if (manifest == null) {
                report.addFatal(Phase.Validation, "Manifest is missing or is not valid xml")
                return report
            }
            // 2. Zip contains '/datasets' directory (Warning, we tolerate without)
            // This check is useless and the code is wrong. Zip format actually does not know directories, even some workarounds exists!
            // Also, you cannot define the requirement that a zip file contains a directory if it could be empty
//            if (zipFile.getEntry("datasets") == null) {
//                report.addWarning(Phase.Validation, "Directory `/datasets` not found")
//            }

            // 3. Manifest valid (protocol.xsd scheme)
            validateManifest(manifest, report)
            // 4. Publisher is from corporate bodies
            val manifestPublisher = manifest.metaInfo().getString("publisher")
            val manifestPublisherConcept = CorporateBodies.getConcept(ModelFactory.createDefaultModel().createResource(manifestPublisher))
            if (manifestPublisherConcept == null) {
                report.addError(Phase.Validation, "Publisher $manifestPublisher in manifest is not a corporate body.")
            }
            // 5. Publisher is same as in fileName
            if (manifestPublisherConcept != null && publisherConcept != null && manifestPublisherConcept.identifier != publisherConcept.identifier) {
                report.addWarning(Phase.Validation, "Publisher $manifestPublisher in manifest is not equal to filename publisher $publisher.")
            }

            manifest.actions().forEach { action ->
                when (action) {
                    is AddReplace -> {
                        // Checks
                        // 1. RDF object exists
                        val obj = zipFile.getObject(action.packagePath)
                        if (obj == null) {
                            report.addError(Phase.Validation, "Action add-replace ${action.id}: Object ${action.packagePath} not found")
                        } else {
                            // 2. RDF syntax
                            try {
                                val reportErrorHandler = ReportErrorHandler(report, "Action add-replace ${action.id}: ")
                                val model = obj.bytes.toModel(Lang.RDFXML, reportErrorHandler)
                                // 3. Semantically integrity
                                if (model.contains(model.createResource(action.objectUri), RDF.type, DCAT.Dataset)) {
                                    val dataset = model.getResource(action.objectUri)

                                    // have publisher
                                    val publisherResource = dataset.getPropertyResourceValue(DCTerms.publisher)
                                    if (publisherResource == null) {
                                        report.addError(Phase.Validation, "Action add-replace ${action.id}: Dataset ${action.objectUri} contains no dct:publisher property")
                                    } else {
                                        if (manifestPublisher != publisherResource.uri) {
                                            report.addError(Phase.Validation, "Action add-replace ${action.id}: Dataset ${action.objectUri} publisher ${publisherResource.uri} is different from manifest publisher $manifestPublisher")
                                        }
                                    }
                                } else if (reportErrorHandler.valid) {
                                    report.addError(Phase.Validation, "Action add-replace ${action.id}: Object ${action.packagePath} contains no dcat:Dataset with uri ${action.objectUri}")
                                }
                            } catch (e: Throwable) {
                                report.addError(Phase.Validation, "Action add-replace ${action.id}: Dataset ${action.packagePath} has invalid rdf: ${e.message}")
                            }
                        }
                    }
                    is Remove -> {
                        // No checks necessary
                    }
                    is UploadDistribution -> {
                        // Checks
                        // 1. Data object exists
                        val data = zipFile.getObject(action.packagePath)
                        if (data == null) {
                            report.addError(Phase.Validation, "Action upload-distribution ${action.id}: Object ${action.packagePath} not found")
                        } else {
                            // 2. Object uri contains correct publisher
                            if (manifestPublisherConcept != null && !action.objectUri.lowercase().contains("/${manifestPublisherConcept.identifier.lowercase()}/")) {
                                report.addWarning(Phase.Validation, "Action upload-distribution ${action.id}: Object uri ${action.objectUri} does not contains publisher ${manifestPublisherConcept.identifier}")
                            }
                        }
                    }
                    else -> {
                        report.addWarning(Phase.Validation, "Unknown action ${action.id}")
                    }
                }
            }
        }
    } catch (e: ZipException) {
        report.addFatal(Phase.Validation, "Zip file: ${e.message}")
    }

    return report
}

fun String.validPackageFileName(): Boolean = fileNameRegex.matches(this)

fun String.decomposeParts(): Pair<String, String> {
    val match = fileNameGroupsRegex.matchEntire(this)
    return when {
        match == null -> "" to ""
        match.groups.size == 1 -> match.groupValues[1] to ""
        match.groups.size >= 2 -> match.groupValues[1] to match.groupValues[2]
        else -> "" to ""
    }
}
