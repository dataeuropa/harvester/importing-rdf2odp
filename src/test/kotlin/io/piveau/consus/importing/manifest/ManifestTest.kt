package io.piveau.consus.importing.manifest

import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.jdom2.input.SAXBuilder
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(VertxExtension::class)
class ManifestTest {

    @Test
    fun `Validate manifest`(vertx: Vertx, testContext: VertxTestContext) {
        val manifest = SAXBuilder().build(vertx.fileSystem().readFileBlocking("manifest.xml").bytes.inputStream())
        val result = validateManifest(manifest)

        testContext.completeNow()
    }

}