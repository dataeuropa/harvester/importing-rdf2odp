package io.piveau.consus.importing.rdf2odp

import io.vertx.junit5.VertxExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(VertxExtension::class)
class RDF2ODPTest {

    @Test
    fun `Test hash generation`() {
        println("http://www.eea.europa.eu/data-and-maps/data/administrative-land-accounting-units".md5())
        println("http://www.eea.europa.eu/data-and-maps/data/administrative-land-accounting-units".euodpHash())
    }

}