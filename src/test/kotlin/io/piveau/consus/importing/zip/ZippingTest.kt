package io.piveau.consus.importing.zip

import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import kotlin.io.path.Path

@ExtendWith(VertxExtension::class)
@Disabled
class ZippingTest {

    @ParameterizedTest
//    @Disabled
    @ValueSource(strings = [
        "COMMU_20210423153552.zip",
        "ESTAT_20210510230117_delete.zip",
        "COMMU_20210510100000.zip",
        "COMP_20210607164432.zip",
        "EMPL_20210617112512_delete.zip",
        "COMMU_20210730150000.zip",
        "ESTAT_20210904230017_updates.zip",
        "COMMU_20210914174000.zip",
        "ESTAT_20210924181911.zip" ])
    fun `Validate package`(packageFile: String, vertx: Vertx, testContext: VertxTestContext) {

        val report = validatePackage(JsonObject()
            .put("fileName", packageFile)
            .put("uploadedFileName", "src/test/resources/archive_import/$packageFile"))
        println(report.prettyPrint())

        testContext.completeNow()
    }

    @Test
    fun `Decompose filename`() {
        println("Valid: ${"ESTAT_20221109_dcatap_testing_2.zip".validPackageFileName()}")
        val (publisher, timestamp) = "ESTAT_20221109_dcatap_testing_2.zip".decomposeParts()
        println("publisher $publisher")
        println("date $timestamp")
//        assertEquals("COMMU", publisher)
//        assertEquals("20210730150000", timestamp)
//        testContext.completeNow()
    }

    @Test
//    @Disabled
    fun `Validate single package`(vertx: Vertx, testContext: VertxTestContext) {

        val report = validatePackage(JsonObject()
            .put("fileName", "ESTAT_20221209110144.zip")
            .put("uploadedFileName", "src/test/resources/ESTAT_20221209110144.zip"))
//        if (!vertx.fileSystem().exists("reports").await()) {
//            vertx.fileSystem().mkdir("reports").await()
//        }
        val reportFileName = report.fileName.removeSuffix(".zip") + ".md"
        vertx.fileSystem()
            .writeFile(
                Path("reports").resolve(reportFileName).toString(),
                Buffer.buffer(report.prettyPrint())
            )
//            .await()
//        println(report.prettyPrint())

        testContext.completeNow()
    }

}